#include "stdafx.h"
#include "Function_Tool.h"

const UINT LOOP_NUM = 1'000'000;
const UINT AVG_LOOP_NUM = 10;

class AAA {
public:
	AAA(int num) : _num(num) {}

	void Func() 
	{
		cout << "AAA::Func : " << _num << endl;
	}

	int _num = 0;
private:
};

void Multiply(int a, int b) 
{
	cout << a * b << endl;
}

int main()
{
	auto func1 = std::bind(Multiply, 4, 5);
	func1();

	auto func2 = std::bind(Multiply, placeholders::_1, placeholders::_2);
	func2(4, 5);

	auto func3 = std::bind(Multiply, 4, placeholders::_1);
	func3(5);

	auto func4 = std::bind(Multiply, placeholders::_1, 4);
	func3(5);

	auto func6 = std::bind(Multiply, placeholders::_2, 4);
	func6(0, 5);

	auto func7 = std::bind(Multiply, 4, placeholders::_2);
	func7(0, 5);

	auto func8 = std::bind(Multiply, 4, placeholders::_3);
	func8(0, 0, 5);


	//멤버 함수 바인딩
	AAA a(10);
	auto memberFunction = std::bind(&AAA::Func, &a);
	memberFunction();
	
	auto memberFunction2 = std::bind(&AAA::Func, std::ref(a));
	memberFunction2();

	//Shared_ptr도 사용 가능하다.
	shared_ptr<AAA> ptrA = make_shared<AAA>(100);
	auto memberFunction3 = std::bind(&AAA::Func, ptrA);
	memberFunction3();

	//멤버 변수 바인딩
	auto aaa = std::bind(&AAA::_num, ptrA);
	cout << aaa() << endl;

}