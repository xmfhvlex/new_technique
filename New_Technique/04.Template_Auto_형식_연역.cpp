
#include "stdafx.h"
#include "Function_Tool.h"

const UINT LOOP_NUM = 1'000'000;
const UINT AVG_LOOP_NUM = 10;

/*
	template<class T>
	void Func (ParamType param) {}
	
	Func(expr);
*/

template<class T>
void Func_ref (T& param) {}

template<class T>
void Func_univ_ref (T&& param) {}

template<class T>
void Func_value (T param) {}

int main()
{
////////////////////////////////////////////////////////////////////////////////////////////////////
//Template 형식 연역 규칙
////////////////////////////////////////////////////////////////////////////////////////////////////
	//1. ParamType이 보편 참조가 아닌 (포인터 or 참조 형식) 인 경우
	//		expr -> ParamType이 될때에는 *, &가 무시된다.
	{
		int n = 10;
		int& rn = n;
		const int cn = 10;
		const int& crn = cn;

		Func_ref(n);	// T : int		 , Param : int &
		Func_ref(rn);	// T : int		 , Param : int &
		Func_ref(cn);	// T : const int , Param : const int &
		Func_ref(crn);	// T : const int , Param : const int &
	}

	//2. ParamType이 보편 참조일 때.
	{
		int n = 10;
		int& rn = n;
		const int cn = 10;
		const int& crn = cn;

		Func_univ_ref(n);	// T : int &		, Param : int & &&			=>	int &		
		Func_univ_ref(rn);	// T : int &		, Param : int & &&			=>	int &		
		Func_univ_ref(cn);	// T : const int &	, Param : const int & &&	=>	const int &
		Func_univ_ref(crn);	// T : const int &	, Param : const int & &&	=>	const int &
		Func_univ_ref(100);	// T : int			, Param : int && &&			=>	int &&	
	}

	//3. ParamType이 값일 떄.
	//		expr 의 const, volatile, & 성질이 무시된다.
	{
		int n = 10;
		int& rn = n;
		const int cn = 10;
		const int& crn = cn;
	
		Func_value(n);		// T : int , Param : int
		Func_value(rn);		// T : int , Param : int
		Func_value(cn);		// T : int , Param : int
		Func_value(crn);	// T : int , Param : int
		Func_value(100);	// T : int , Param : int
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Auto 형식 연역 규칙.
////////////////////////////////////////////////////////////////////////////////////////////////////
		//1. ParamType이 보편 참조가 아닌 (포인터 or 참조 형식) 인 경우
	//		expr -> ParamType이 될때에는 *, &가 무시된다.
	{
		int n = 10;
		const int cn = 10;
		auto& ref1 = n;
		auto& ref2 = cn;
	}

	//2. ParamType이 보편 참조일 때.
	{
		int n = 10;
		const int cn = 10;
		auto&& uref1 = n;		// Param : int & 
		auto&& uref2 = cn;		// Param : const int &
		auto&& uref2 = 10;		// Param : int &&
	}

	//3. ParamType이 값일 떄.
	//		expr 의 const, volatile, & 성질이 무시된다.
	{
		auto n = 10;
		auto cn = n;		// Param : int
	}
}