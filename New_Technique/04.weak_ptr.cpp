#include "stdafx.h"
#include "Function_Tool.h"

const UINT LOOP_NUM = 1'000'000;
const UINT AVG_LOOP_NUM = 10;

template<class T>
void f(const T& data) {
	cout << data << endl;
}

class A {
public:
	void Print() {
		cout << "A" << endl;
	}
};

int main() {

	f(10);

	std::shared_ptr<int> ptrShared = make_shared<int>(100);
	std::weak_ptr<int> ptrWeak;


	//Weak Ptr은 Reference Count를 증가시키지 않는다.
	cout << *ptrShared << " " << ptrShared.use_count() << endl;
	ptrWeak = ptrShared;
	cout << *ptrShared << " " << ptrShared.use_count() << endl;
	cout << *ptrWeak.lock() << " " << ptrWeak.use_count() << endl;

	

	// weak ptr이 참조중인 ptrShared가 null이 된다.
	ptrShared = nullptr;

	// 따라서 weak ptr은 사용전 참조중인 포인터가 만료되었는지를 확인해야 한다.
	if (ptrWeak.expired() == true) {
		cout << "weak ptr 만료" << endl;
	}

	cout << "-------------------------------------------" << endl;

	std::shared_ptr<A> ptrShared2 = make_shared<A>(new A);
	std::weak_ptr<A> ptrWeak2;
	ptrWeak2 = ptrShared2;

	ptrWeak2.lock()->Print();
}