#include "stdafx.h"
#include "Function_Tool.h"

const UINT LOOP_NUM = 1'000'000;
const UINT AVG_LOOP_NUM = 10;

/*
	문자열에 대한 비소유 참조(non- owning reference).

	이 클래스의 주된 용도는 "const string &"을 대체 하기 위함.
*/

void println(const string& str)
{
	//cout << str << endl;
}

void println_strv(const string_view& str)
{
	//cout << str << endl;
}

int main()
{
	//문자열 메모리가 새로 할당되는 문제가 없는 경우. (아무런 문제가 없다)
	string str("Hello Guys");
	println(str);

	//const char* 가 string 클래스의 생성자로 전달되면서 실행은 되지만 메모리 할당과 문자열 복사가 발생한다. (성능 하락 문제)
	println("Hello Guys");

	//------------------------------------------------------------------------------------------------------------------------------------------
	//성능 테스트
	CStopWatch watch;

	// 사용된 시간 : 0.001244[0.0012442]
	watch.BeginTimer();
	for (volatile int i = 0; i < LOOP_NUM; ++i) {
		println(str);
	}
	watch.EndTimer();

	// 사용된 시간 : 0.001639[0.0016393]		// 메모리 할당으로 인한 확실한 성능 저하.
	watch.BeginTimer();
	for (volatile int i = 0; i < LOOP_NUM; ++i) {
		println("Hello Guys");
	}
	watch.EndTimer();

	// 사용된 시간 : 0.001234[0.0012343]
	watch.BeginTimer();
	for (volatile int i = 0; i < LOOP_NUM; ++i) {
		println_strv(str);
	}
	watch.EndTimer();

	// 사용된 시간 : 0.001237[0.0012373]
	watch.BeginTimer();
	for (volatile int i = 0; i < LOOP_NUM; ++i) {
		println_strv("Hello Guys");
	}
	watch.EndTimer();

	//------------------------------------------------------------------------------------------------------------------------------------------
	// 문자열 리터럴 
	auto str1 = "Hello Guys";	//const char * 
	auto str2 = "Hello Guys"sv;	//string_view

	//------------------------------------------------------------------------------------------------------------------------------------------
	// 추가 기능
	string_view strv("Hello Guys");
	strv.remove_prefix(2);	//앞 두 문자 제거
	strv.remove_suffix(2);	//뒤 두 문자 제거
	cout << strv << endl;

}