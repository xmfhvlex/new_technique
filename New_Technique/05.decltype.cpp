
#include "stdafx.h"
#include "Function_Tool.h"

const UINT LOOP_NUM = 1'000'000;
const UINT AVG_LOOP_NUM = 10;
/*
	일반적인 Container 들은 색인 접근을 하면 & 형태로 반환한다.
	Func2 함수에서 & 를 유지하면서 반환하기 위해 container[index]와 똑같은 형식을 연역하도록 gksek. 
 */

// C++11 부터 사용 가능한 형식 연역 반환 문법.
template<class Container, class Index>
auto Func1(Container& container, Index index) -> decltype(container[index]) 
{
	return container[index];
}

// C++14 부터 사용 가능한 형식 연역 반환 문법.
template<class Container, class Index>
decltype(auto) Func2(Container& container, Index index)
{
	return container[index];
}

int main()
{
	decltype(auto) n1 = 10;				// ParamType : int
	decltype(auto) n2 = n1;				// ParamType : int
	decltype(auto) n3 = (n1);			// ParamType : int &
	decltype(auto) n4 = std::move(10);	// ParamType : int &&

	int& rn = n1;
	decltype(auto) n5 = rn;				// ParamType : int &

	vector<bool> b;
	decltype(auto) bb = b[0];
}