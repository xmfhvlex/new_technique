#include "stdafx.h"
#include "Function_Tool.h"

const UINT LOOP_NUM = 1'000'000;
const UINT AVG_LOOP_NUM = 10;

/*
	유연성과 명확성 측면에서 함수 포인터보다 바람직하다(약간의 오버헤드).

	vector<int&> a;				 ====> 불가
	vector<vector<int> & >  vv;  ====> 불가

	vector<vector<int> * >  vv;  ====> 가능
	위와 같이 포인터를 사용할 순 있지만. 생포인터를 사용하는 것은 지양.

	레퍼런스의 컨테이너를 만들 수 없지만.
	다음과 같이 레퍼런스의 컨테이너와 같은 기능을 구현할 수 있다.
	vector<std::reference_wrapper<vector<int>>> vv;
*/

int main()
{
	vector<int> v1{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	vector<int> v2{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	vector<int> v3{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	vector<int> v4{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	vector<std::reference_wrapper<vector<int>>> vv;
	vv.push_back(v1);
	vv.push_back(v2);
	vv.push_back(v3);
	vv.push_back(v4);

		//다음과 같은형태도 가능하다.
		vector<decltype(std::ref(v1))> vv2;
	
	for (const vector<int>& vr : vv) {
		for (const int& n : vr)
			cout << n << " ";
		cout << endl;
	}
}