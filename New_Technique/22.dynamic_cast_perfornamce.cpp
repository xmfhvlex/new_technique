#include "stdafx.h"
#include "Function_Tool.h"

const UINT LOOP_NUM = 1'000'000;
const UINT AVG_LOOP_NUM = 10;

class A {
public:
	virtual ~A(){};
};

class B : public A {
	virtual ~B() {};
};

/*
	Debug
	0.2

	Release
	0.047
	0.038
*/

int main() {
	CStopWatch watch;

	duration<double> emptyAvgTime = duration<double>(0);
	{
		duration<double> avgTime = duration<double>(0);
		for (volatile int avgLoop = 0; avgLoop < AVG_LOOP_NUM; ++avgLoop) {
			watch.BeginTimer();
			for (volatile int i = 0; i < LOOP_NUM; ++i) {

				A* pA = new B();
				B* pB = dynamic_cast<B*>(pA);

			}
			avgTime += watch.EndTimerNoPrint();
		}
		avgTime /= AVG_LOOP_NUM;
		avgTime -= emptyAvgTime;
		cout << "AVG Time : " << to_string(avgTime.count()) << endl << endl;
	}

	emptyAvgTime = duration<double>(0);
	{
		duration<double> avgTime = duration<double>(0);
		for (volatile int avgLoop = 0; avgLoop < AVG_LOOP_NUM; ++avgLoop) {
			watch.BeginTimer();
			for (volatile int i = 0; i < LOOP_NUM; ++i) {

				B* pB = new B();
				A* pA = pB;

			}
			avgTime += watch.EndTimerNoPrint();
		}
		avgTime /= AVG_LOOP_NUM;
		avgTime -= emptyAvgTime;
		cout << "AVG Time : " << to_string(avgTime.count()) << endl << endl;
	}
}
