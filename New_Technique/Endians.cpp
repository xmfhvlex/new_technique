//#include "stdafx.h"
//#include "Function_Tool.h"
//
//int main()
//{
//	//unsigned int a = 4294967295;
//	unsigned int a = 1;
//	byte* pByte = (byte*)&a;
//
//	printf("%d\n", pByte[0]);// << " - " << pByte[1] << " - " << pByte[2] << " - " << pByte[3] << endl;
//	printf("%d\n", pByte[1]);
//	printf("%d\n", pByte[2]);
//	printf("%d\n", pByte[3]);
//
//	byte arr[4];
//	arr[0] = pByte[3];
//	arr[1] = pByte[2];
//	arr[2] = pByte[1];
//	arr[3] = pByte[0];
//
//	unsigned int pInt = (int&)*arr;
//	cout << pInt << endl;
//
//
//	int num = 0x01234567;
//	byte byteNum = *((byte*)&num);
//
//	if (byteNum == 0x67) {
//	/*
//		Little Endian 의 경우.
//		byte[0] = 0x67
//		byte[1] = 0x45
//		byte[2] = 0x23
//		byte[3] = 0x01
//		순으로 저장된다.
//
//		높은 자리수가 높은 번지수에 저장된다. High High
//		낮은 자리수가 낮은 번지수에 저장된다. Low Low
//	*/
//		cout << "Little" << endl;
//
//	}
//	else {
//	/*
//		Big Endian 의 경우.
//		byte[0] = 0x01
//		byte[1] = 0x23
//		byte[2] = 0x45
//		byte[3] = 0x67
//		순으로 저장된다.
//	*/
//		cout << "Big" << endl;
//	}
//
//
//
//	cout << 0x67 << endl;
//
//}

#include "stdafx.h"
#include "Function_Tool.h"

/*int main() {
	int num = 0;
	cin >> num;

	for (int i = 0; i < num; ++i) {
		unsigned int inputNum = 0;
		cin >> inputNum;

		char* arrByte;
		arrByte = (char*) & inputNum;

		char arrSwappedByte[4];
		arrSwappedByte[0] = arrByte[3];
		arrSwappedByte[1] = arrByte[2];
		arrSwappedByte[2] = arrByte[1];
		arrSwappedByte[3] = arrByte[0];

		cout << *((unsigned int*)arrSwappedByte) << endl;
	}

}*/


//#include <iostream>
//#include <map>
//#include <algorithm>
//using namespace std;
//
//int main() {
//	int num = 0;
//	cin >> num;
//
//	for (int i = 0; i < num; ++i) {
//		map<int, int> mX;
//		map<int, int> mY;
//		for (int j = 0; j < 3; ++j) {
//			int x, y;
//			cin >> x >> y;
//			mX[x]++;
//			mY[y]++;
//		}
//
//		int resultX, resultY;
//		for (auto data : mX) {
//			if (data.second == 1)
//				resultX = data.first;
//		}
//		for (auto data : mY) {
//			if (data.second == 1)
//				resultY = data.first;
//		}
//		cout << resultX << " " << resultY << endl;
//	}
//
//}


//#include <iostream>
//#include <algorithm>
//#include <vector>
//#include <string>
//using namespace std;
//
//void sortAndPrint(const string& str) {
//	vector<string> vStr;
//	for (int i = 0; i < str.length(); i += 2) {
//		string tempStr = "";
//		for (int j = 0; j < 2; ++j) {
//			tempStr += str[i + j];
//		}
//		vStr.push_back(tempStr);
//	}
//	sort(vStr.begin(), vStr.end());
//	for (const string& data : vStr)
//		cout << data;
//	cout << endl;
//}
//
//int main() {
//	int num;
//	cin >> num;
//
//	for (int i = 0; i < num; ++i) {
//		string str;
//		cin >> str;
//		sortAndPrint(str);
//	}
//}


#include <iostream>
#include <string>
using namespace std;

int main() {


	try
	{
		throw(10);
	}
	catch (int expr) {
		cout << expr << endl;

	}
	//int arr[10];

	//vector<int> vec(10);
	//try
	//{
	////	cout << arr[10] << endl;
	//	vec.at(10);
	//}
	//catch(out_of_range){
	//	cout << "zz" << endl;
	//}


	//try {
	//	mapNum.at("zzz");
	//}
	//catch (runtime_error) {
	//	cout << "runtime_error" << endl;
	//}
	//catch (out_of_range) {
	//	cout << "out_of_range" << endl;
	//}
	//catch (...) {
	//	cout << "..." << endl;
	//}
	cout << endl;
	cout << endl;

	int num;
	cin >> num;

	for (int i = 0; i < num; ++i) {
		float value;
		cin >> value;
		string type;
		cin >> type;

		if (type == "kg") {
			printf("%d %.04f lb", i + 1, value * 2.2046);
		}
		else if (type == "lb") {
			printf("%d %.04f kg", i + 1, value * 0.4536);
		}
		else if (type == "l") {
			printf("%d %.04f g", i + 1, value * 0.2642);
		}
		else if (type == "g") {
			printf("%d %.04f l", i + 1, value * 3.7854);
		}
		cout << endl;
	}

}