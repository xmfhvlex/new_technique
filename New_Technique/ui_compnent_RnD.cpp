
#include "stdafx.h"
#include "Function_Tool.h"

class IUIComponent {
public:
	virtual void Update() {}
	virtual void Render() {}
};


template<typename T>
class UIString : public IUIComponent {

public:
	UIString(T& data) {
		_data = data;
	}
	UIString(T&& data) {
		_data = data;
	}

	void Render() override {
		Func(_data);
	//	Func(std::forward<T>(data));
	}

	//--------------------------------------------------------------------
	template<typename T2>
	void Func(T2&& a) {
		cout << "NONE" << endl;
	}

	template<typename T2>
	void Func(T2* a) {
		cout << "NONE" << endl;
	}

	template<typename T2>
	void Func(T2** a) {
		cout << "NONE" << endl;
	}

	//--------------------------------------------------------------------
	template<>
	void Func(int* a) {
		cout << "INT * : " << a << endl;
		(*a)++;
	}

	template<>
	void Func(float* a) {
		cout << "FLOAT * : " << a << endl;
	}

	template<>
	void Func(std::string* a) {
		cout << "STRING * : " << a << endl;
	}

	//--------------------------------------------------------------------
	template<>
	void Func(int** a) {
		cout << "INT * : " << a << endl;
	}

	template<>
	void Func(float** a) {
		cout << "FLOAT * : " << a << endl;
	}

	template<>
	void Func(std::string** a) {
		cout << "STRING * : " << a << endl;
	}

	//--------------------------------------------------------------------
	template<>
	void Func(int& a) {
		cout << "INT ref : " << a << endl;
	}

	template<>
	void Func(float& a) {
		cout << "FLOAT ref : " << a << endl;
	}

	template<>
	void Func(std::string& a) {
		cout << "STRING ref : " << a << endl;
	}

	//--------------------------------------------------------------------
	template<>
	void Func(int&& a) {
		cout << "INT right : " << a << endl;
	}

	template<>
	void Func(float&& a) {
		cout << "FLOAT right : " << a << endl;
	}

	template<>
	void Func(std::string&& a) {
		cout << "STRING right : " << a << endl;
	}

protected:
	T _data;
};


class IRendererComponent {
public:
	virtual void Operate() {
		cout << "-----------------------------" << endl;
	}

private:
};

class ContainerRenderer : public IRendererComponent {
public:
	void Operate() override {
		IRendererComponent::Operate();
		cout << "Container-----------------------------" << endl;
	}

private:
};

class StringRenderer : public IRendererComponent {
public:
	void Operate() override {
		IRendererComponent::Operate();
		cout << "Container-----------------------------" << endl;
	}

private:
};

const UINT LOOP_NUM = 1'000'000;
const UINT AVG_LOOP_NUM = 10;

int main()
{
	IRendererComponent*a = new ContainerRenderer;
	a->Operate();

	cout << "-----------------------------" << endl;
	int num = 10;
	std::vector<IUIComponent*> vComp;

	vComp.push_back(new UIString<int>(100));
	vComp.push_back(new UIString<int>(num));
	vComp.push_back(new UIString<int*>(&num));
	vComp.push_back(new UIString<float>(2.123f));
	vComp.push_back(new UIString<std::string>("Hahahah"));
//	vComp.push_back(new UIString<bool>(true));

	for (auto comp : vComp)
		comp->Render();


	cout << "num : " << num << endl;
}

//template <typename T, unsigned int row, unsigned int col>
//class Matrix
//{
//public:
//	static Matrix<T, row, col> zero;
//
//public:
//	Matrix(void) { T c = T(0); for (unsigned int i = 0; i < row; ++i) for (unsigned int j = 0; j < col; ++j) data[i][j] = c; }
//	~Matrix(void) { }
//
//	Matrix<T, row, col>& operator = (const Matrix<T, row, col>& matrix);
//
//public:
//	T data[row][col];
//};
//
//template <typename T, unsigned int row, unsigned int col>
//Matrix<T, row, col>& Matrix<T, row, col>::operator=(const Matrix<T, row, col>& matrix)
//{
//	// some processes..  
//}
//
//template <typename T, unsigned int row, unsigned int col>
//Matrix<int, row, col>& Matrix<int, row, col>::operator=(const Matrix<int, row, col>& matrix)
//{
//	// some processes..  
//}